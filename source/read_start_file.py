#! /usr/bin/env python3
# -*- coding: utf-8 -*-

class StartSettings:
	def __init(self):
		self.x_half_size = 0.0
		self.x_size_tumba = 0.0
		sefl.y_size_bed = 0.0
		self.wood_width = 0.0
		self.wood_row_count_base = 0
		self.wood_col_count_base = 0
		self.wood_height_row_count = 0
		self.wood_height = 0.0
		self.up_slip = 0.0
		self.y_size_tumba = 0.0
		self.wood_col_count_up = 0

	def read_file(self, fn):
		all_data = []

		f = open(fn, 'r')

		for line in f:
			all_data.append(line)
		f.close()

		# print (all_data)

		start_row_ind = 2
		step = 2

		self.x_half_size = float(all_data[start_row_ind])
		self.x_size_tumba = float(all_data[start_row_ind + 1 * step])
		self.y_size_bed = float(all_data[start_row_ind + 2 * step])
		self.wood_width = float(all_data[start_row_ind + 3 * step])
		self.wood_row_count_base = int(all_data[start_row_ind + 4 * step])
		self.wood_col_count_base = int(all_data[start_row_ind + 5 * step])
		self.wood_height_row_count = int(all_data[start_row_ind + 6 * step])
		self.wood_height = float(all_data[start_row_ind + 7 * step])
		self.up_slip = float(all_data[start_row_ind + 8 * step])
		self.y_size_tumba = float(all_data[start_row_ind + 9 * step])
		self.wood_col_count_up = int(all_data[start_row_ind + 10 * step])
		self.wood_row_count_up = int(all_data[start_row_ind + 11 * step])

