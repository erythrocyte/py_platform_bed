#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import read_start_file
from read_start_file import StartSettings

import commonClasses
from commonClasses import *

from math import sqrt


def make_all_points(grd, ss):
	# base
	si = 0
	sloy_ind = 0
	for i in range(ss.wood_col_count_base + 1):
		[grd, si] = make_points_sloy(grd, ss, False, sloy_ind, si)
		sloy_ind = sloy_ind + 1

	for i in range(ss.wood_col_count_up + 1):
		[grd, si] = make_points_sloy(grd, ss, True, sloy_ind, si)
		sloy_ind = sloy_ind + 1

	return grd

def make_points_sloy(grd, ss, is_up, sloy_ind, start_index):
	z = sloy_ind * ss.wood_height
	first_region_node_count = 0
	x_slip = 0
	if (is_up):
		first_region_node_count = ss.wood_row_count_up + 1
		x_slip = ss.up_slip
		
	else:
		first_region_node_count = ss.wood_row_count_base + 1

	second_region_node_count = 2 * first_region_node_count
	third_region_node_count = first_region_node_count
	forth_region_node_count = second_region_node_count
	fifth_region_node_count = first_region_node_count
	sixth_region_node_count = first_region_node_count

	[grd, start_index] = make_first_region_nodes(ss, grd, first_region_node_count, start_index, z, x_slip)
	[grd, start_index] = make_second_region_nodes(ss, grd, second_region_node_count, start_index, z, x_slip)
	[grd, start_index] = make_third_region_nodes(ss, grd, third_region_node_count, start_index, z, x_slip)
	[grd, start_index] = make_forth_region_nodes(ss, grd, forth_region_node_count, start_index, z, x_slip)
	[grd, start_index] = make_fifth_region_nodes(ss, grd, fifth_region_node_count, start_index, z, x_slip)
	[grd, start_index] = make_sixth_region_nodes(ss, grd, sixth_region_node_count, start_index, z, x_slip)

	return [grd, start_index]


def make_first_region_nodes(ss, grd, first_region_node_count, start_index, z, x_slip):
	# first region nodes
	x0 = 0.0 - ss.x_half_size
	y0 = 0.0
	for i in range(first_region_node_count):
		nd = Node()
		nd.ind = start_index
		start_index = start_index + 1
		nd.x = x0 + i * sqrt(2.0) * ss.wood_width - x_slip
		nd.y = y0 + i * sqrt(2.0) * ss.wood_width
		nd.z = z
		grd.nodes.append(nd)

	return [grd, start_index]

def make_second_region_nodes(ss, grd, second_region_node_count, start_index, z, x_slip):
	[grd, start_index] = make_second_region_nodes_a(ss,grd,second_region_node_count, start_index, z, x_slip)
	[grd, start_index] = make_second_region_nodes_b(ss,grd,second_region_node_count, start_index, z, x_slip)
	return [grd, start_index]

def make_second_region_nodes_a (ss, grd, second_region_node_count, start_index, z, x_slip):
	#second region nodes, a
	x0 = 0.0 - ss.x_half_size
	y0 = ss.y_size_tumba
	for i in range(int(second_region_node_count / 2)):
		nd = Node()
		nd.ind = start_index
		start_index = start_index + 1
		nd.x = x0 + i * sqrt(2.0) * ss.wood_width - x_slip
		nd.y = y0 - i * sqrt(2.0) * ss.wood_width
		nd.z = z
		grd.nodes.append(nd)

	return [grd, start_index]

def make_second_region_nodes_b (ss, grd, second_region_node_count, start_index, z, x_slip):
	#second region nodes, b
	x0 = 0.0 - ss.x_half_size + ss.x_size_tumba
	y0 = ss.y_size_tumba
	for i in range(int(second_region_node_count / 2)):
		nd = Node()
		nd.ind = start_index
		start_index = start_index + 1
		nd.x = x0 + i * sqrt(2.0) * ss.wood_width - x_slip
		nd.y = y0 - i * sqrt(2.0) * ss.wood_width
		nd.z = z
		grd.nodes.append(nd)

	return [grd, start_index]

def make_third_region_nodes(ss, grd, third_region_node_count, start_index, z, x_slip):
	#third region nodes
	x0 = ss.x_half_size
	y0 = 0.0
	for i in range(third_region_node_count):
		nd = Node()
		nd.ind = start_index
		start_index = start_index + 1
		nd.x = x0 - i * sqrt(2.0) * ss.wood_width + x_slip
		nd.y = y0 + i * sqrt(2.0) * ss.wood_width
		nd.z = z
		grd.nodes.append(nd)

	return [grd, start_index]

def make_forth_region_nodes(ss, grd, forth_region_node_count, start_index, z, x_slip):
	[grd, start_index] = make_forth_region_nodes_a(ss, grd, forth_region_node_count, start_index, z, x_slip)
	[grd, start_index] = make_forth_region_nodes_b(ss, grd, forth_region_node_count, start_index, z, x_slip)
	return [grd, start_index]

def make_forth_region_nodes_a (ss, grd, second_region_node_count, start_index, z, x_slip):
	#second region nodes, a
	x0 = ss.x_half_size
	y0 = ss.y_size_tumba
	for i in range(int(second_region_node_count / 2)):
		nd = Node()
		nd.ind = start_index
		start_index = start_index + 1
		nd.x = x0 - i * sqrt(2.0) * ss.wood_width + x_slip
		nd.y = y0 - i * sqrt(2.0) * ss.wood_width
		nd.z = z
		grd.nodes.append(nd)

	return [grd, start_index]

def make_forth_region_nodes_b(ss, grd, second_region_node_count, start_index, z, x_slip):
	#second region nodes, b
	x0 = ss.x_half_size - ss.x_size_tumba
	y0 = ss.y_size_tumba
	for i in range(int(second_region_node_count / 2)):
		nd = Node()
		nd.ind = start_index
		start_index = start_index + 1
		nd.x = x0 - i * sqrt(2.0) * ss.wood_width + x_slip
		nd.y = y0 - i * sqrt(2.0) * ss.wood_width
		nd.z = z
		grd.nodes.append(nd)

	return [grd, start_index]

def make_fifth_region_nodes(ss, grd, fifth_region_node_count, start_index, z, x_slip):
	x0 = -ss.x_half_size + ss.x_size_tumba
	y0 = ss.y_size_tumba + ss.y_size_bed
	for i in range(fifth_region_node_count):
		nd = Node()
		nd.ind = start_index
		start_index = start_index + 1
		nd.x = x0 + i * sqrt(2.0) * ss.wood_width - x_slip
		nd.y = y0 - i * sqrt(2.0) * ss.wood_width
		nd.z = z
		grd.nodes.append(nd)

	return [grd, start_index]

def make_sixth_region_nodes(ss, grd, sixth_region_node_count, start_index, z, x_slip):
	x0 = ss.x_half_size - ss.x_size_tumba
	y0 = ss.y_size_tumba + ss.y_size_bed
	for i in range(sixth_region_node_count):
		nd = Node()
		nd.ind = start_index
		start_index = start_index + 1
		nd.x = x0 - i * sqrt(2.0) * ss.wood_width + x_slip
		nd.y = y0 - i * sqrt(2.0) * ss.wood_width
		nd.z = z
		grd.nodes.append(nd)

	return [grd, start_index]
