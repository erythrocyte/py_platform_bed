# ~/usr/bin/env python3
# -*- coding: utf-8 -*-

class Grid:
	def __init__(self):
		self.nodes = []
		self.faces = []
		self.cells = []
		self.one_sloy_base_faces_count = 0
		self.one_sloy_up_faces_count = 0

class Node:
	def __init__(self):
		self.x = 0.0
		self.y = 0.0
		self.z = 0.0
		self.ind = 0

class Face:
	def __init__(self):
		self.ind = 0
		self.nodes = []


class Cell:
	def __init__(self):
		self.ind = 0
		self.nodes = []
