#! /usr/bin/env python3
# -*- coding: utf-8 -*-

def make_bok_faces_for_flat_face(ss, grd, fi, sloy_ind, si):
	b = False if sloy_ind <= ss.wood_col_count_base+1 else True
	sloy_point_count = get_one_sloy_point_count(ss, b)

	fc = grd.faces[fi]
	lni = len(fc.nodes)

	for i in range(lni-1):
		fc1 = Face()
		fc1.ind = si
		si += 1
		fc.nodes.append(fc.nodes[i])
		fc.nodes.append(fc.nodes[i+1])
		fc.nodes.append(fc.nodes[i+sloy_point_count+1])
		fc.nodes.append(fc.nodes[i+sloy_point_count])

		grd.faces.append(fc)
	return [grd, si]
