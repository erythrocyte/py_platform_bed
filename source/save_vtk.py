#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import commonClasses
from commonClasses import Grid

# ============== example ======================
	# # vtk DataFile Version 3.0
	# vtk output
	# ASCII
	# DATASET UNSTRUCTURED_GRID
	# POINTS 4 float
	# 0 0 0
	# 1 0 0
	# 0 1 0
	# 1.1 1.1 0
	# CELLS 1 5
	# 4 0 1 3 2
	# CELL_TYPES 1
	# 9
	# CELL_DATA 1
	# POINT_DATA 4
	# FIELD FieldData 1
	# nodal 1 4 float
	# 0 1 1.1 2
# =============================================

class VtkSaver:
	def __init(self):
		self.mode = 0

	def write_global_header(self, f):
		f.write('# vtk DataFile Version 3.0\n')
		f.write('vtk output\n')
		f.write('ASCII\n')
		f.write('DATASET UNSTRUCTURED_GRID\n')

	def write_global_points(self, f, grd):
		f.write('POINTS {} float\n'.format(len(grd.nodes)))
		for nd in grd.nodes:
			f.write('{} {} {}\n'.format(nd.x, nd.y, nd.z))

	def write_cells_as_points(self, f, grd):
		l = len(grd.nodes)
		f.write('CELLS {} {}\n'.format(l, l * 2))
		for nd in grd.nodes:
			f.write('1 {}\n'.format(nd.ind))

	def write_global_cells_type(self, f, cells_count, cell_type):
		f.write('CELL_TYPES {}\n'.format(cells_count))
		for i in range(cells_count):
			f.write('{}\n'.format(cell_type))

	def write_cells_data_as_point(self, f, grd):
		f.write('CELL_DATA {}\n'.format(len(grd.nodes)))
		f.write('SCALARS ind int 1\n')
		f.write('LOOKUP_TABLE default\n')

		for nd in grd.nodes:
			f.write('{}\n'.format(nd.ind))

	def write_cells_as_faces(self, f, grd, v_fi):
		lv_fi = len(v_fi)
		l = len(grd.faces) if lv_fi == 0 else lv_fi
		f.write('CELLS {} {}\n'.format(l, l * 5))
		if (lv_fi == 0):
			for fc in grd.faces:
				v_fi.append(fc.ind)

		for lfi in v_fi:
			fc = grd.faces[lfi]
			s = '4 '
			for ni in fc.nodes:
				s += str(ni) + ' '
			s.strip()
			f.write('{}\n'.format(s))


	def write_cells_data_as_faces(self, f, grd, v_fi):
		lv_fi = len(v_fi)
		l = len(grd.faces) if lv_fi == 0 else lv_fi
		f.write('CELL_DATA {}\n'.format(l))
		f.write('SCALARS ind int 1\n')
		f.write('LOOKUP_TABLE default\n')

		if (lv_fi == 0):
			for fc in grd.faces:
				v_fi.append(fc.ind)

		for lfi in v_fi:
			f.write('{}\n'.format(lfi))


# =========================================================

def save_vtk_for_points(grd, fn):
	vtk_svr = VtkSaver()
	f = open (fn, 'w')
	vtk_svr.write_global_header(f)
	vtk_svr.write_global_points(f, grd)
	vtk_svr.write_cells_as_points( f, grd)
	vtk_svr.write_global_cells_type(f, len(grd.nodes), 1)
	vtk_svr.write_cells_data_as_point(f, grd)
	f.close()

def save_vtk_for_faces(grd, fn, v_fi):
	vtk_svr = VtkSaver()
	f = open (fn, 'w')
	vtk_svr.write_global_header(f)
	vtk_svr.write_global_points(f, grd)
	vtk_svr.write_cells_as_faces(f, grd, v_fi)
	lv_fi = len(v_fi)
	l = len(grd.faces) if lv_fi == 0 else lv_fi
	vtk_svr.write_global_cells_type(f, l, 9)
	vtk_svr.write_cells_data_as_faces(f, grd, v_fi)
	f.close()

# ==============================================================
