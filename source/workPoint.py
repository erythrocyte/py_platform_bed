#! /usr/bin/env python3
# -*- coding: utf-8 -*-

def get_points_ai(ss):
	ai = []
	for i in range(ss.wood_col_count_base+1):
		s = get_one_sloy_point_count(ss, False)
		ai.append(s)
	
	for i in range(ss.wood_col_count_up+1):
		s = get_one_sloy_point_count(ss, False)
		ai.append(s)

	return ai

	
def get_one_sloy_point_count(ss, is_up):
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base 
	# zone1
	s1 = row_count + 1
	# zone2
	s2 = 2 * (row_count + 1)
	#zone3
	s3 = s1
	# zone4
	s4 = s2
	# zone5
	s5 = s1
	# zone5
	s6 = s1

	s = s1 + s2 + s3 + s4 + s5 + s6
	return s

def get_point_count_till_sloy(ss, z_step, ai):
	sum_ind = 0
	for i in range(z_step-1):
		sum_ind += ai[i]

		# b = False if i <= ss.wood_col_count_base+1 else True
		# cf = 0 if i == 1 else 1
		# one_sloy_points = get_one_sloy_point_count(ss, b)
		# sloy_point_count = cf * one_sloy_points
		# sum_ind += sloy_point_count
		# print ('for sloy[{}] point count is {}, ind = {}'.format(i, one_sloy_points, sloy_point_count))
		# print ('sum_ind = {}'.format(sum_ind))

	# print ('sum point count = {}'.format(sum_ind))
	return sum_ind
