#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import read_start_file
from read_start_file import StartSettings

import commonClasses
from commonClasses import *

from math import sqrt

import workPoint
from workPoint import *

import makeBokFaces
from makeBokFaces import make_bok_faces_for_flat_face


def make_all_faces(grd, ss):
	ai = []
	ai = get_points_ai(ss)


	si = 0
	sloy_ind = 1
	for i in range(ss.wood_col_count_base + 1):
		[grd, si] = make_one_sloy(grd, ss, False, sloy_ind, si, ai)
		# [grd, si] = make_one_sloy(grd, ss, True, sloy_ind, si)
		sloy_ind += 1
		# if i > 0:
			# break

	grd.one_sloy_base_faces_count = len(grd.faces)

	# return grd
	print (sloy_ind)

	# sloy_ind = sloy_ind - 1
	for i in range(ss.wood_col_count_up + 1):
		[grd, si] = make_one_sloy(grd, ss, True, sloy_ind, si, ai)
		sloy_ind += 1
		# if i >0:
			# break;

	grd.one_sloy_up_faces_count = len(grd.faces) - grd.one_sloy_base_faces_count


	sloy_ind = 1
	for i in range(ss.wood_col_count_base + 1):
		makeBokFaces.make_bok_faces_for_flat_face(ss, grd

	for i in range(ss.wood_col_count_up + 1):

	return grd


def make_one_sloy(grd, ss, is_up, sloy_i, si, ai):
	[grd, si] = make_faces_zone_one(grd, sloy_i, ss, is_up, si, ai)
	[grd, si] = make_faces_zone_two(grd, sloy_i, ss, is_up, si, ai)
	[grd, si] = make_faces_zone_three(grd, sloy_i, ss, is_up, si, ai)
	[grd, si] = make_faces_zone_four(grd, sloy_i, ss, is_up, si, ai)
	[grd, si] = make_faces_zone_fifth(grd, sloy_i, ss, is_up, si, ai)
	[grd, si] = make_faces_zone_sixth(grd, sloy_i, ss, is_up, si, ai)
	[grd, si] = make_faces_zone_seventh(grd, sloy_i, ss, is_up, si, ai)
	[grd, si] = make_faces_zone_eight(grd, sloy_i, ss, is_up, si, ai)
	return [grd, si]

def make_faces_zone_one(grd, z_step, ss, is_up, start_index, ai):
	k0 = 0
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base
	sloy_point_count = get_one_sloy_point_count(ss, is_up)
	sum_ind = get_point_count_till_sloy(ss, z_step, ai)
	for i in range(row_count):
		fc = Face()
		fc.ind = start_index
		start_index += 1
		fc.nodes.append(k0 + i + sum_ind)
		k1 = (row_count + 1) + 2 * (row_count + 1) + i + sum_ind
		fc.nodes.append(k1)
		fc.nodes.append(k1+1)
		fc.nodes.append(k0 + 1 + i + sum_ind)
		grd.faces.append(fc)

	return [grd, start_index]

def make_faces_zone_two(grd, z_step, ss, is_up, start_index, ai):
	k0 = 0
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base
	sloy_point_count = get_one_sloy_point_count(ss, is_up)
	# sum_ind = sloy_point_count * z_step
	sum_ind = get_point_count_till_sloy(ss, z_step, ai)
	# print ('sum_ind = {}, z_step = {}'.format(sum_ind, z_step))
	for i in range(row_count):
		fc = Face()
		fc.ind = start_index
		# print ('start_index = {}'.format(start_index))
		start_index += 1
		k00 = k0 + i + sum_ind
		fc.nodes.append(k00)
		fc.nodes.append(k00 + 1)
		k1 = (row_count + 1) + 1 + i + sum_ind
		fc.nodes.append(k1)
		fc.nodes.append(k1-1)
		grd.faces.append(fc)


	return [grd, start_index]

def make_faces_zone_three(grd, z_step, ss, is_up, start_index, ai):
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base
	k0 = row_count + 1
	sloy_point_count = get_one_sloy_point_count(ss, is_up)
	# sum_ind = sloy_point_count * z_step
	sum_ind = get_point_count_till_sloy(ss, z_step, ai)
	for i in range(row_count):
		fc = Face()
		fc.ind = start_index
		start_index += 1
		k00 = k0 + i + sum_ind
		fc.nodes.append(k00)
		fc.nodes.append(k00 + 1)
		k1 = k0 + (row_count + 1) + 1 + i + sum_ind
		fc.nodes.append(k1)
		fc.nodes.append(k1-1)
		grd.faces.append(fc)


	return [grd, start_index]

def make_faces_zone_four(grd, z_step, ss, is_up, start_index, ai):
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base
	k0 = 2 * (row_count + 1)
	sloy_point_count = get_one_sloy_point_count(ss, is_up)
	# sum_ind = sloy_point_count * z_step
	sum_ind = get_point_count_till_sloy(ss, z_step, ai)
	for i in range(row_count):
		fc = Face()
		fc.ind = start_index
		start_index += 1
		k00 = k0 + i + sum_ind
		fc.nodes.append(k00)
		fc.nodes.append(k00 + 1)
		k1 = k0 + 4 * (row_count + 1) + 1 + i + sum_ind
		fc.nodes.append(k1)
		fc.nodes.append(k1-1)
		grd.faces.append(fc)


	return [grd, start_index]

def make_faces_zone_fifth(grd, z_step, ss, is_up, start_index, ai):
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base
	k0 = 6 * (row_count + 1)
	sloy_point_count = get_one_sloy_point_count(ss, is_up)
	# sum_ind = sloy_point_count * z_step
	sum_ind = get_point_count_till_sloy(ss, z_step, ai)
	for i in range(row_count):
		fc = Face()
		fc.ind = start_index
		start_index += 1
		k00 = k0 + i + sum_ind
		fc.nodes.append(k00)
		fc.nodes.append(k00 + 1)
		k1 = k0 + 1 * (row_count + 1) + 1 + i + sum_ind
		fc.nodes.append(k1)
		fc.nodes.append(k1-1)
		grd.faces.append(fc)


	return [grd, start_index]

def make_faces_zone_sixth(grd, z_step, ss, is_up, start_index, ai):
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base
	k0 = 7 * (row_count + 1)
	sloy_point_count = get_one_sloy_point_count(ss, is_up)
	# sum_ind = sloy_point_count * z_step
	sum_ind = get_point_count_till_sloy(ss, z_step, ai)
	for i in range(row_count):
		fc = Face()
		fc.ind = start_index
		start_index += 1
		k00 = k0 + i + sum_ind
		fc.nodes.append(k00)
		fc.nodes.append(k00 + 1)
		k1 = k0 - 2 * (row_count + 1) + 1 + i + sum_ind
		fc.nodes.append(k1)
		fc.nodes.append(k1-1)
		grd.faces.append(fc)


	return [grd, start_index]

def make_faces_zone_seventh(grd, z_step, ss, is_up, start_index, ai):
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base
	k0 = 5 * (row_count + 1)
	sloy_point_count = get_one_sloy_point_count(ss, is_up)
	# sum_ind = sloy_point_count * z_step
	sum_ind = get_point_count_till_sloy(ss, z_step, ai)
	for i in range(row_count):
		fc = Face()
		fc.ind = start_index
		start_index += 1
		k00 = k0 + i + sum_ind
		fc.nodes.append(k00)
		fc.nodes.append(k00 + 1)
		k1 = k0 - 1 * (row_count + 1) + 1 + i + sum_ind
		fc.nodes.append(k1)
		fc.nodes.append(k1-1)
		grd.faces.append(fc)
	return [grd, start_index]

def make_faces_zone_eight(grd, z_step, ss, is_up, start_index, ai):
	row_count = ss.wood_row_count_up if is_up else ss.wood_row_count_base
	k0 = 4 * (row_count + 1)
	sloy_point_count = get_one_sloy_point_count(ss, is_up)
	# sum_ind = sloy_point_count * z_step
	sum_ind = get_point_count_till_sloy(ss, z_step, ai)
	for i in range(row_count):
		fc = Face()
		fc.ind = start_index
		start_index += 1
		k00 = k0 + i + sum_ind
		fc.nodes.append(k00)
		fc.nodes.append(k00 + 1)
		k1 = k0 - 1 * (row_count + 1) + 1 + i + sum_ind
		fc.nodes.append(k1)
		fc.nodes.append(k1-1)
		grd.faces.append(fc)
	return [grd, start_index]
