#! /usr/bin/env python3
# -*- coding: utf=8 -*-

import sys
sys.path.insert(0, 'source/')

import read_start_file
from read_start_file import StartSettings

import commonClasses
from commonClasses import Grid

import makePoints
from makePoints import make_all_points

import save_vtk
from save_vtk import save_vtk_for_points, save_vtk_for_faces

import makeFaces
from makeFaces import make_all_faces, get_point_count_till_sloy

import makeBokFaces
from makeBokFaces import make_bok_faces_for_flat_face


def main():
	main_start_file_name = 'st.dat'
	ss = StartSettings()
	ss.read_file(main_start_file_name)

	# l = (ss.wood_col_count_up+1) + (ss.wood_col_count_base+1)
	# get_point_count_till_sloy(ss, l)
	# return

	grd = Grid()
	grd = makePoints.make_all_points(grd, ss)
	grd = makeFaces.make_all_faces(grd, ss)

	print ('node count = {}'.format(len(grd.nodes)))
	print ('face count = {}'.format(len(grd.faces)))

	save_vtk.save_vtk_for_points(grd, 'nodes.vtk')

	v_fi = []
	# [v_fi.append(d) for d in range(grd.one_sloy_base_faces_count)]
	# v_fi.append(grd.one_sloy_base_faces_count-1)
	# v_fi.append(grd.one_sloy_base_faces_count)
	# v_fi.append(grd.one_sloy_base_faces_count+1)
	save_vtk.save_vtk_for_faces(grd, 'faces.vtk', v_fi)

if __name__ == '__main__':
	main()


